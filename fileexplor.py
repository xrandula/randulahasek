import tkinter as tk
import tkinter.ttk as ttk
import os

root = tk.Tk()
root.geometry("800x600")
root.iconbitmap("img/icon_fileexplorer.ico")
root.title("Dual L Explorer 2022")

style = ttk.Style(root)
style.configure("Treeview",
                font=("Bold", 14))

drives = ["A://", "B://", "C://",
          "D://", "E://", "F://",
          "G://", "H://", "I://",
          "J://", "K://", "L://",
          "M://", "N://", "O://",
          "P://", "Q://", "R://",
          "S://", "T://", "U://",
          "V://", "W://", "X://",
          "Y://", "Z://"]

valid_drives = []

browser_dir = []

def insert_drives():
    for i in side_table.get_children():
        side_table.delete(i)

    for r in range(len(valid_drives)):
        side_table.insert(parent="", iid=r, text="", values=[valid_drives[r]], index="end")

def find_valid_drives():
    for drive in drives:
        if os.path.exists(drive):
            valid_drives.append(drive)

    insert_drives()

def insert_folders(path):
    global browser_dir
    for i in main_table.get_children():
        main_table.delete(i)

    folders = os.listdir(path)

    browser_dir = []

    for r in range(len(folders)):
        main_table.insert(parent="", iid=r, text="", values=[folders[r]], index="end")
        browser_dir.append(str(path) + "/" + folders[r])

def open_drive():
    index = int(side_table.selection()[0])

    path = valid_drives[index]
    insert_folders(path)

def insert_files(path):
    global browser_dir
    for i in main_table.get_children():
        main_table.delete(i)

    files = os.listdir(path)
    browser_dir = []

    for r in range(len(files)):
        main_table.insert(parent="", iid=r, text="", index="end", values=[files[r]])
        browser_dir.append(str(path) + "/" + files[r])

def open_folder():
    index = int(main_table.selection()[0])

    path = browser_dir[index]

    if os.path.isdir(path):
        insert_files(path)
    else:
        os.system('"%s"' % path)


side_table = ttk.Treeview(root)

side_table["column"] = ["Drivers"]
side_table.column("#0", anchor=tk.W, width=0, stretch=tk.NO)
side_table.column("Drivers", anchor=tk.W, width=100, stretch=tk.NO)
side_table.heading("Drivers", text="Disky", anchor=tk.W)
side_table.pack(side=tk.LEFT, anchor=tk.W, fill=tk.Y)
side_table.bind("<<TreeviewSelect>>", lambda e: open_drive())

main_table = ttk.Treeview(root)

main_table["column"] = ["Soubory"]
main_table.column("#0", anchor=tk.W, width=0, stretch=tk.NO)
main_table.column("Soubory", anchor=tk.W, width=700)
main_table.heading("Soubory", text="Soubory", anchor=tk.W)
main_table.pack(side=tk.LEFT, anchor=tk.W, fill=tk.Y)
main_table.bind("<<TreeviewSelect>>", lambda e: open_folder())

find_valid_drives()
root.mainloop()