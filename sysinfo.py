from tkinter import *
import platform



sysOkno = Tk()
sysOkno.title("Vlastnosti systému")
sysOkno.iconbitmap(r"img/Logo.ico")
sysOkno.geometry("600x230")


InfoBlog = f"System:  \n\
{platform.system()}\n \
{platform.version()}\n \
{platform.architecture()}\n \
Registrované: \n \
{platform.machine()} \n \
Počítač : \n \
{platform.processor()} \n \
"

SysText = Label(sysOkno, text=InfoBlog, font=("Arial", 14))
SysText.pack()

sysOkno.mainloop()