
import turtle as t
import tkinter as tk
hracAskore = 0
hracBskore = 0

obrazovka = t.Screen()


obrazovka.title("Ping pong")
obrazovka.bgcolor("#99CC00")
obrazovka.setup(width = 800, height = 600)
obrazovka.tracer(0)

#levy obdelnik

levyObdelnik = t.Turtle()
levyObdelnik.speed(0)
levyObdelnik.shape("square")
levyObdelnik.color("#333333")
levyObdelnik.shapesize(stretch_wid = 6,stretch_len = 1)
levyObdelnik.penup()
levyObdelnik.goto(-350, 0)

#pravy obdelnik

pravyObdelnik = t.Turtle()
pravyObdelnik.speed(0)
pravyObdelnik.shape("square")
pravyObdelnik.shapesize(stretch_wid = 6,stretch_len = 1)
pravyObdelnik.color("#333333")
pravyObdelnik.penup()
pravyObdelnik.goto(350, 0)

#micek
micek = t.Turtle()
micek.speed(0)
micek.shape("circle")
micek.color("#333333")
micek.penup()
micek.goto(0, 0)
micekX = 0.2
micekY = 0.2

#skore
pero = t.Turtle()
pero.speed(0)
pero.color("#333333")
pero.penup()
pero.hideturtle()
pero.goto(0, 260)
pero.write("Hrac A:{}     Hrac B:{}".format(hracAskore, hracBskore), align = 'center', font = ("Courier", 24, "bold"))





#pohyb leveho obdelniku

def levyObdelnikNahoru():
    y = levyObdelnik.ycor()
    if y < 240:
        y = y + 45
    levyObdelnik.sety(y)

def levyObdelnikDolu():
    y = levyObdelnik.ycor()
    if y > -240:
        y = y - 45
    levyObdelnik.sety(y)



#pohyb praveho obdelniku

def pravyObdelnikNahoru():
    y = pravyObdelnik.ycor()
    if y < 240:
        y = y + 45
    pravyObdelnik.sety(y)

def pravyObdelnikDolu():
    y = pravyObdelnik.ycor()
    if y > -240:
        y = y - 45
    pravyObdelnik.sety(y)
#konec hry
def konec():
    global program
    program = False

canvas = obrazovka.getcanvas()
tlacitkoKonec = tk.Button(canvas.master, text = "Konec", bg = "#333333", fg = "white", width = 10, command = konec)
canvas.create_window(340, -280, window = tlacitkoKonec)

program = True
#ovladani
obrazovka.listen()
obrazovka.onkeypress(levyObdelnikNahoru, "w")
obrazovka.onkeypress(levyObdelnikDolu, "s")
obrazovka.onkeypress(pravyObdelnikNahoru, "Up")
obrazovka.onkeypress(pravyObdelnikDolu, "Down")




while program:
    obrazovka.update()


    #pohyb micku
    micek.setx(micek.xcor() + micekX)
    micek.sety(micek.ycor() + micekY)

    #hranice
    if micek.ycor() > 290:
        micek.sety(290)
        micekY = micekY * -1

    if micek.ycor() < -290:
        micek.sety(-290)
        micekY = micekY * -1

    if micek.xcor() > 390:
        micek.goto(0, 0)
        micekX = micekX * -1
        hracAskore = hracAskore + 1
        pero.clear()
        pero.write("Hrac A:{}     Hrac B:{}".format(hracAskore, hracBskore), align = 'center', font = ("Courier", 24, "bold"))


    if micek.xcor() < -390:
        micek.goto(0, 0)
        micekX = micekX * -1
        hracBskore = hracBskore + 1
        pero.clear()
        pero.write("Hrac A:{}     Hrac B:{}".format(hracAskore, hracBskore), align='center', font=("Courier", 24, "bold"))


        #kolize
    if(micek.xcor() > 340)  and (micek.xcor() < 350) and (micek.ycor() < pravyObdelnik.ycor() + 50 and micek.ycor() > pravyObdelnik.ycor() - 50):
        micek.setx(340)
        micekX = micekX * -1


    if(micek.xcor() < -340)  and (micek.xcor() > -350) and (micek.ycor() < levyObdelnik.ycor() + 50 and micek.ycor() > levyObdelnik.ycor() - 50):
        micek.setx(-340)
        micekX = micekX * -1






