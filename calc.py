from tkinter import *
from tkinter.filedialog import *
import tkinter as tk
# Nastavení okna
oknocal = Tk()
oknocal.geometry("312x324")
oknocal.resizable(0,0)
oknocal.title("Kalkulačka - DualSys L 2022")
oknocal.iconbitmap(r"img/icon_calculator.ico")

# pár detailu

def hClick(item):
    global expression
    expression = expression + str(item)
    input_text.set(expression)

def hVycisti():
    global expression
    expression = ""
    input_text.set("")

def hVys():
    global expression
    result = str(eval(expression))
    input_text.set(result)
    expression = ""

expression = ""

input_text = StringVar()
input_frame = Frame(oknocal, width=312, height=50, bd=0, highlightbackground="black", highlightcolor="black", highlightthickness=2)
input_frame.pack(side=TOP)

input_field = Entry(input_frame, font=('arial', 18, 'bold'), textvariable=input_text, width=50, bg="#eee", bd=0, justify=RIGHT)
input_field.grid(row=0, column=0)
input_field.pack(ipady=10)
tlac_frame = Frame(oknocal, width=312, height=272.5, bg="grey")
tlac_frame.pack()

vycistit = Button(tlac_frame, text = "C", fg = "black", width = 32, height = 3, bd = 0, bg = "#eee", cursor = "hand2", command = lambda: hVycisti()).grid(row = 0, column = 0, columnspan = 3, padx = 1, pady = 1)
vydel = Button(tlac_frame, text = "/", fg = "black", width = 10, height = 3, bd = 0, bg = "#eee", cursor = "hand2", command = lambda: hClick("/")).grid(row = 0, column = 3, padx = 1, pady = 1)
sedum = Button(tlac_frame, text="7", fg="black", width=10, height=3, bd=0, bg="#fff", cursor="hand2",
               command=lambda: hClick(7)).grid(row=1, column=0, padx=1, pady=1)

osem = Button(tlac_frame, text="8", fg="black", width=10, height=3, bd=0, bg="#fff", cursor="hand2",
               command=lambda: hClick(8)).grid(row=1, column=1, padx=1, pady=1)

devet = Button(tlac_frame, text="9", fg="black", width=10, height=3, bd=0, bg="#fff", cursor="hand2",
              command=lambda: hClick(9)).grid(row=1, column=2, padx=1, pady=1)

krat = Button(tlac_frame, text="*", fg="black", width=10, height=3, bd=0, bg="#eee", cursor="hand2",
                  command=lambda: hClick("*")).grid(row=1, column=3, padx=1, pady=1)


ctyry = Button(tlac_frame, text="4", fg="black", width=10, height=3, bd=0, bg="#fff", cursor="hand2",
              command=lambda: hClick(4)).grid(row=2, column=0, padx=1, pady=1)

pet = Button(tlac_frame, text="5", fg="black", width=10, height=3, bd=0, bg="#fff", cursor="hand2",
              command=lambda: hClick(5)).grid(row=2, column=1, padx=1, pady=1)

sest = Button(tlac_frame, text="6", fg="black", width=10, height=3, bd=0, bg="#fff", cursor="hand2",
             command=lambda: hClick(6)).grid(row=2, column=2, padx=1, pady=1)

minus = Button(tlac_frame, text="-", fg="black", width=10, height=3, bd=0, bg="#eee", cursor="hand2",
               command=lambda: hClick("-")).grid(row=2, column=3, padx=1, pady=1)



jedna = Button(tlac_frame, text="1", fg="black", width=10, height=3, bd=0, bg="#fff", cursor="hand2",
             command=lambda: hClick(1)).grid(row=3, column=0, padx=1, pady=1)

dva = Button(tlac_frame, text="2", fg="black", width=10, height=3, bd=0, bg="#fff", cursor="hand2",
             command=lambda: hClick(2)).grid(row=3, column=1, padx=1, pady=1)

tri = Button(tlac_frame, text="3", fg="black", width=10, height=3, bd=0, bg="#fff", cursor="hand2",
               command=lambda: hClick(3)).grid(row=3, column=2, padx=1, pady=1)

plus = Button(tlac_frame, text="+", fg="black", width=10, height=3, bd=0, bg="#eee", cursor="hand2",
              command=lambda: hClick("+")).grid(row=3, column=3, padx=1, pady=1)


nula = Button(tlac_frame, text="0", fg="black", width=21, height=3, bd=0, bg="#fff", cursor="hand2",
              command=lambda: hClick(0)).grid(row=4, column=0, columnspan=2, padx=1, pady=1)

carka = Button(tlac_frame, text=".", fg="black", width=10, height=3, bd=0, bg="#eee", cursor="hand2",
               command=lambda: hClick(".")).grid(row=4, column=2, padx=1, pady=1)

vysledek = Button(tlac_frame, text="=", fg="black", width=10, height=3, bd=0, bg="#eee", cursor="hand2",
                command=lambda: hVys()).grid(row=4, column=3, padx=1, pady=1)

oknocal.mainloop()
