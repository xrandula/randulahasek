from tkinter import *
from tkinter.filedialog import *
import tkinter as tk

# barvy
textcolor = "Black"
backcolor = "White"


# vymoženosti
def vycistitSoubor():
    vstup.delete(1.0, END)


def ulozSoubor():
    novy_soubor = asksaveasfile(mode="w", filetype=[("Textový soubor", ".txt"), ("Hypertext Markup Language", ".html"),
                                                    ("Kaskádové styly", ".css")])
    if novy_soubor is None:
        return
    text = str(vstup.get(1.0, END))
    novy_soubor.write(text)
    novy_soubor.close()


def OtevrSoubor():
    soubor = askopenfile(mode="r", filetype=[("Textový soubor", "*.txt"), ("Hypertext Markup Language", "*.html"),
                                             ("Kaskádové styly", "*.css")])
    if soubor is not None:
        content = soubor.read()
    vstup.insert(INSERT, content)


#Grafické styly
def ClassicShell():
    vstup.config(bg="White", fg="Black")
    okno.config(bg="White")


def NightClassic():
    vstup.config(bg="Black", fg="White")
    okno.config(bg="Gray")

def Hacker():
    vstup.config(bg="Black", fg="#2bff00")
    okno.config(bg="Green")

def BlueWorld():
    vstup.config(bg="#004fcf", fg="#00cfbd")
    okno.config(bg="Blue")

def PinkGlasses():
    vstup.config(bg="#ba00cf", fg="#edafea")
    okno.config(bg="Pink")

def BlueHacker():
    vstup.config(bg="Black", fg="Aqua")
    okno.config(bg="Aqua")

#Utility
def Kopiruj():
    vstup.event_generate("<<Copy>>")

def Ufikni():
    vstup.event_generate("<<Cut>>")

def VlozKopirku():
    vstup.event_generate("<<Paste>>")

def Zpatecka():
    vstup.edit_undo()
    vstup.config()

def Predecka():
    vstup.edit_redo()
    vstup.config()


#Notepad :D
okno = tk.Tk()
menu = Menu(okno)
okno.config(menu=menu)
okno.geometry("1024x800")
okno.title("Notepad - DualSys L 2022")
okno.iconbitmap(r"img/icon_notepad.ico")
okno.config(bg="White")

subMenu1 = Menu(menu)
menu.add_cascade(label="Soubor", menu=subMenu1)
subMenu1.add_command(label="Otevřít soubor", command=OtevrSoubor)
subMenu1.add_command(label="Uložit", command=ulozSoubor)
subMenu1.add_command(label="Vyčistit", command=vycistitSoubor)

subMenu2 = Menu(menu)
menu.add_cascade(label="Zobrazení", menu=subMenu2)
subMenu2.add_command(label="Classic", command=ClassicShell)
subMenu2.add_command(label="Classic Night", command=NightClassic)
subMenu2.add_command(label="Hacker", command=Hacker)
subMenu2.add_command(label="Blue World", command=BlueWorld)
subMenu2.add_command(label="Pink Glasses", command=PinkGlasses)
subMenu2.add_command(label="Blue Hacker", command=BlueHacker)

subMenu3 = Menu(menu)
menu.add_cascade(label="Editace", menu=subMenu3)
subMenu3.add_command(label="Kopírovat", command=Kopiruj)
subMenu3.add_command(label="Vložit", command=VlozKopirku)
subMenu3.add_command(label="Oddělej", command=Ufikni)
subMenu3.add_command(label="O krok vpřed", command=Zpatecka)
subMenu3.add_command(label="O krok zpět", command=Predecka)

menu.add_cascade(label="Ukončit", command=exit)

vstup = Text(okno, wrap=WORD, bg=backcolor, fg=textcolor, font=("arial", 27))
vstup.pack(padx=2, pady=2, expand=TRUE, fill=BOTH)

okno.mainloop()
