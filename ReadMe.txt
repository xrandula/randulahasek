Jedná se o aplikační a herní balíček.
Může být užíván jako nahrazení aplikací OS. 
Byl vytvořen jako projekt do Pythonu v roce 2022
Vytvořil Libor Hašek a Libor Randula

Verze 0.15 Obsahuje 
-Hry
-> Ping Pong
-> Had 3310

-Aplikace 
-> Notepad
-> Kalkulačka
-> Prohlížeč Dokumentů

-Start možnosti 
-> O nás / O aplikaci 
-> Tento počítač
-> Vypnout

V Notepadu máte více možnosti vzhledu.
Na ploše běží aktualní čas.

Minimalní požadavky 
CPU : Pentium 4 2,4 Ghz+
HDD : 0,2 GB
RAM : 256 MB 
GPU : nVidia Geforce 4+ 
