import tkinter as tK
import tkinter.messagebox
from tkinter import *
import os
import subprocess
from datetime import datetime

from time import strftime

now = datetime.now()

#programy
#import notepad
def OtevriNotepad():
    subprocess.call(["python", "notepad.py"])

#import had
def OtevriHada():
    subprocess.call(["python", "had.py"])
#import pong
def OtevriPong():
    subprocess.call(["python", "pong.py"])

#import kalkulačka
def OtevriCalc():
    subprocess.call(["python", "calc.py"])

#import sysinfo
def SystemInfo():
    subprocess.call(["python", "sysinfo.py"])

#čas
def cas():
    string = strftime("%H:%M:%S")
    hcas.config(text=string)
    hcas.after(1000, cas)
def fileexpl():
    subprocess.call(["python","fileexplor.py"])
#oNás
def onas():
    tkinter.messagebox.showinfo("O DualSysL 2022", "Jedná se o aplikační a herní balíček.\nMůže být užívan jako nahrazení aplikací Operačního systému.\nByl vytvořen jako projekt Pythonu 2022.\nVytvořil Libor Hašek a Libor Randula\n\nVerze 0.15" )







OknoMenu = Tk()
menu = Menu(OknoMenu)
OknoMenu.geometry("600x320")
OknoMenu.title("DualSysL 2022")
OknoMenu.iconbitmap(r"img/Logo.ico")
OknoMenu.config(menu=menu)
OknoMenu.configure(background="#008080")
Logo = PhotoImage(file="img/Logo.png")
Pozadi1 = PhotoImage(file="img/duall_wallpaper.png")
IconNotepad = PhotoImage(file="img/icon_notepad.png")
IconSnake = PhotoImage(file="img/icon_snake.png")
IconCalculator = PhotoImage(file="img/icon_calculator.png")
IconPong = PhotoImage(file="img/icon_pong.png")
IconFE = PhotoImage(file="img/icon_fileexplorer.png")
LLogo = Label(OknoMenu, image=Logo)
topFrame = Frame(OknoMenu)
topFrame.pack()
bottomFrame = Frame(OknoMenu)
bottomFrame.pack(side=BOTTOM)


#Start Menu
start = Menu(menu,background="silver",fg="black")
menu.add_cascade(label="START", menu=start)
start.add_command(label="Programy")
start.add_command(label="O nás / O aplikaci", command=onas)
start.add_command(label="Nastavení")
start.add_command(label="Prohlížeč dokumentů", command=fileexpl)
start.add_command(label="Příkazový řádek")
start.add_command(label="Tento počítač",command=SystemInfo)
start.add_command(label="Vypnout", command=exit)

##menu.add_cascade(command=TimeInCascade)


tlacitko1 = Button(text="Notepad",bg="#008090",fg="white", image=IconNotepad, compound=TOP, command=OtevriNotepad)
tlacitko1.pack(side=LEFT)
tlacitko1.place(x=5,y=5)

tlacitko2 = Button( text="Ping pong", bg="#008090",fg="white", image=IconPong, compound=TOP, command=OtevriPong)
tlacitko2.pack(side=LEFT)
tlacitko2.place(x=5,y=100)


tlacitko3 = Button(text="Had 3310",bg="#008090",fg="white", image=IconSnake, compound=TOP, command=OtevriHada)
tlacitko3.pack(side=LEFT)
tlacitko3.place(x=5,y=195)

tlacitko4 = Button(text="Kalkulačka",bg="#008090",fg="white", image=IconCalculator, compound=TOP, command=OtevriCalc)
tlacitko4.pack(side=RIGHT)
tlacitko4.place(x=80,y=5)

tlacitko5 = Button(text="Prohlížeč\nDokumentů",bg="#008090",fg="white", image=IconFE, compound=TOP, command=fileexpl)
tlacitko5.pack(side=RIGHT)
tlacitko5.place(x=80,y=100)




LLogo.pack(side=TOP)

hcas = Label(OknoMenu, font=("Arial", 25), bg="#008080", fg="White")
hcas.pack(anchor="center")
cas()



OknoMenu.mainloop()