import turtle
import random
import time
import tkinter as tk


obrazovka = turtle.Screen()
canvas = obrazovka.getcanvas()




def konecHry():

    tlacitkoKonec = tk.Button(canvas.master, text = "Konec hry", bg = "#333333", fg = "white", width=30, command = exit)
    tlacitkoNovaHra = tk.Button(canvas.master, text = "Nova hra", bg = "#333333", fg="white", width=30, command = novaHra)
    canvas.create_window(150, 50, window = tlacitkoKonec)
    canvas.create_window(-150, 50, window = tlacitkoNovaHra)


    turtle.done()
def novaHra():
    obrazovka.clear()
    hra()

def hra():
    # hlavni obrazovka

    obrazovka.title("Had 3310")
    obrazovka.setup(width = 700, height = 700)
    obrazovka.tracer(0)
    obrazovka.bgcolor("#99CC00")



    # hranice
    turtle.speed(5)
    turtle.pensize(4)
    turtle.penup()
    turtle.goto(-310, 250)
    turtle.pendown()
    turtle.color("#333333")
    turtle.forward(600)
    turtle.right(90)
    turtle.forward(500)
    turtle.right(90)
    turtle.forward(600)
    turtle.right(90)
    turtle.forward(500)
    turtle.penup()
    turtle.hideturtle()

    # skore
    skore = 0
    zpozdeni = 0.1

    # telo hada
    had = turtle.Turtle()
    had.speed()
    had.shape("square")
    had.color("#333333")
    had.penup()
    had.goto(0, 0)
    had.direction = "stop"

    # jidlo

    ovoce = turtle.Turtle()
    ovoce.speed(0)
    ovoce.shape("circle")
    ovoce.color("#333333")
    ovoce.penup()
    ovoce.goto(30, 30)

    stareOvoce = []

    # vysledek

    vysledek=turtle.Turtle()
    vysledek.speed(0)
    vysledek.color("#333333")
    vysledek.penup()
    vysledek.hideturtle()
    vysledek.goto(0, 300)
    vysledek.write("Skore: ", align = "Center", font = ("Courier", 24, "bold"))

    # pohyb hada
    def hadNahoru():
        if had.direction != "dolu":
            had.direction = "nahoru"

    def hadDolu():
        if had.direction != "nahoru":
            had.direction = "dolu"

    def hadDoleva():
        if had.direction != "doprava":
            had.direction = "doleva"

    def hadDoprava():
        if had.direction != "doleva":
            had.direction = "doprava"

    def pohybHada():
        if had.direction == "nahoru":
            y = had.ycor()
            had.sety(y + 20)
        if had.direction == "dolu":
            y = had.ycor()
            had.sety(y - 20)
        if had.direction == "doleva":
            x = had.xcor()
            had.setx(x - 20)
        if had.direction == "doprava":
            x = had.xcor()
            had.setx(x + 20)

    # klavesy
    obrazovka.listen()
    obrazovka.onkeypress(hadNahoru, "Up")
    obrazovka.onkeypress(hadDolu, "Down")
    obrazovka.onkeypress(hadDoleva, "Left")
    obrazovka.onkeypress(hadDoprava, "Right")

    # hlavni
    while True:
        obrazovka.update()

        # had vezme ovoce
        if(had.distance(ovoce)) < 16:
            x = random.randint(-290, 270)
            y = random.randint(-240, 240)
            ovoce.goto(x, y)
            vysledek.clear()
            skore += 1
            vysledek.write("Skore: {}".format(skore), align="Center", font=("Courier", 24, "bold"))
            zpozdeni -= 0.001

            # vytvareni noveho jidla
            noveOvoce = turtle.Turtle()
            noveOvoce.speed(0)
            noveOvoce.shape("square")
            noveOvoce.color("#333333")
            noveOvoce.penup()
            stareOvoce.append(noveOvoce)

        # rust hada

        for index in range(len(stareOvoce) -1, 0, -1):
            a = stareOvoce[index - 1].xcor()
            b = stareOvoce[index - 1].ycor()

            stareOvoce[index].goto(a, b)
        if len(stareOvoce) > 0:
            a = had.xcor()
            b = had.ycor()
            stareOvoce[0].goto(a, b)
        pohybHada()

        #kolize s hranici
        if had.xcor() > 280 or had.xcor() < -300 or had.ycor() > 240 or had.ycor() < -240:
            time.sleep(1)
            obrazovka.clear()
            obrazovka.bgcolor("#99CC00")
            vysledek.goto(0, 0)
            vysledek.write("Hra skoncila \nVase skore: {}".format(skore), align="center", font=("Courier", 30, "bold"))
            konecHry()


        #kolize hada

        for jidlo in stareOvoce:
            if jidlo.distance(had) < 16:
                time.sleep(1)
                obrazovka.clear()
                obrazovka.bgcolor("#99CC00")
                vysledek.goto(0, 0)
                vysledek.write("Hra skoncila \nVase skore: {}".format(skore), align = "center", font = ("Courier", 30, "bold"))


                konecHry()



        time.sleep(zpozdeni)




hra()















